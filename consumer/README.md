# Testing tree shaking
This folder acts as a sandbox to verify that tree shaking works when the lumere-ui library is consumed.

## Installation
From the project root, run:
```
npm install
npm run build
npm pack
cd consumer && npm install ../lumere-lumere-ui-0.7.1.tgz
```
## Building and verifying
Run the build process:

```
npm run build
```

Built files can be examined at `/dist/` to verify tree shaking is working as expected.

## Testing unpublished changes
Unpublished changes can always be tested by running the build process and then creating a tarball with npm pack. See the installation step or [npm-pack](https://docs.npmjs.com/cli/v7/commands/npm-pack) for more information.

# Demo app

This folder also has a dev server that can be used to verify CSS is working as expected:

```
npm run dev
```

