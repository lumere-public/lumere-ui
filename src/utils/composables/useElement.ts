import { tryOnScopeDispose, noop } from '@vueuse/shared';
import { unref, watch } from 'vue';
import type { Ref } from 'vue';

export function useElement(el: HTMLElement | Ref<HTMLElement | null> | null, onMounted: (el: HTMLElement) => void, onUnmounted: () => void) {
    let cleanup = noop;

    const stopWatch = watch(
        () => unref(el),
        (el) => {
            cleanup();
            if (!el) {return;}

            onMounted(unref(el));

            cleanup = () => {
                onUnmounted();
                cleanup = noop;
            };
        },
        { immediate: true, flush: 'post' },
    );

    const stop = () => {
        stopWatch();
        cleanup();
    };

    tryOnScopeDispose(stop);

    return stop;
}
