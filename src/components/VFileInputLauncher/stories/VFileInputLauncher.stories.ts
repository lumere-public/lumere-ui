import { vueStory } from '@vueStory';
import { VFileInputLauncher } from '../index.js';
import VModelBasedUsageStory from './VModelBasedUsage.story.vue';
import EventBasedUsageStory from './EventBasedUsage.story.vue';
import CustomizingLauncherElementStory from './CustomizingLauncherElement.story.vue';
import UnsavedChangesStory from './UnsavedChanges.story.vue';

export default {
    title: 'Vue/Components/VFileInputLauncher',
    component: VFileInputLauncher,
};

export const VModelBasedUsage = vueStory(VModelBasedUsageStory);
export const EventBasedUsage = vueStory(EventBasedUsageStory);
export const CustomizingLauncherElement = vueStory(CustomizingLauncherElementStory);
export const UnsavedChanges = vueStory(UnsavedChangesStory);
