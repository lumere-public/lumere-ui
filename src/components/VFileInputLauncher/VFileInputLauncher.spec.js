import { shallowMount } from '@vue/test-utils';
import { vi } from 'vitest';
import { VFileInputLauncher } from './index';

describe('testing VFileInputLauncher.vue', () => {
    it('can have its tag customized', () => {
        const defaultLauncher = shallowMount(VFileInputLauncher);
        const anchorLauncher = shallowMount(VFileInputLauncher, {
            propsData: {
                tag: 'a',
            },
        });
        expect(defaultLauncher.element.tagName).toBe('BUTTON');
        expect(anchorLauncher.element.tagName).toBe('A');
    });

    it('can prevent launching the file input when inputCallback returns false', async() => {
        const wrapper = shallowMount(VFileInputLauncher);
        const launcherSpy = vi.spyOn(wrapper.vm.$refs.fileInput, 'click');
        const launcherButton = wrapper.find('button');

        // no inputCallback specified, file input will be launched
        launcherButton.trigger('click');
        expect(launcherSpy).toHaveBeenCalledTimes(1);

        // inputCallback returns false, file input will not be launched
        await wrapper.setProps({ inputCallback: () => false });
        launcherButton.trigger('click');
        expect(launcherSpy).toHaveBeenCalledTimes(1);

        // inputCallback returns true, file input will be launched
        await wrapper.setProps({ inputCallback: () => true });
        launcherButton.trigger('click');
        expect(launcherSpy).toHaveBeenCalledTimes(2);
    });
});
