<template>
    <Transition name="popover">
        <div :class="['v-popover bypass-unsaved-warning popover-top', { 'v-popover-arrowless': !arrow }]"
            ref="popoverEl">
            <div v-if="arrow" ref="arrowEl" :class="['popover-arrow', { 'v-popover-arrow-gray': isFooterArrow }]"></div>
            <div class="popover-content" v-if="isActive">
                <div v-if="$slots.header" class="popover-header">
                    <slot name="header"></slot>
                </div>
                <div v-if="$slots.body" class="popover-body">
                    <slot name="body"></slot>
                </div>
                <div v-if="$slots.footer" class="popover-footer">
                    <slot name="footer"></slot>
                </div>
            </div>
        </div>
    </Transition>
</template>

<script setup lang="ts">
    import { ref, computed, useSlots, onMounted, nextTick, Ref, PropType } from 'vue';
    import { createPopper, OptionsGeneric, Modifier, StrictModifiers, Boundary, RootBoundary, Placement } from '@popperjs/core';
    import { onClickOutside, onKeyStroke } from '@vueuse/core';

    const isActive = ref(false);
    const props = defineProps({
        /** String describing the placement of the popover in relation to the reference element. See https://popper.js.org/docs/v2/constructors/#options for the full list of options */
        position: { type: String as PropType<Placement>, default: 'top' },
        /** Popper modifier to control/show the arrow for the popover. See https://popper.js.org/docs/v2/modifiers/arrow/ for the full arrow spec */
        arrow: { type: [Boolean, Object], default: true },
        /** Popper modifier to control the popover's offsets. See https://popper.js.org/docs/v2/modifiers/offset/ for the full offset spec */
        offset: { type: Array as unknown as PropType<[number, number]>, default: () => ([0, 10]) },
        /** Popper modifier to specify the popover's placement in the case it starts to overlap its reference element. See https://popper.js.org/docs/v2/modifiers/flip/ for the full spec */
        flip: { type: Object, default: () => ({}) },
        /** Controls if the popover will appear on top of the trigger element */
        coverTrigger: { type: Boolean, default: false },
        refreshPosition: { type: Boolean, default: false },
        toggleable: { type: Boolean, default: true },
        /** Decides if it will attempt to scroll the popover to view */
        scrollToView: { type: Boolean, default: false },
        /** Returning true will prevent the popover from closing when clicking outside */
        onClickOutside: { type: Function as PropType<(event: EventTarget) => boolean>, default: () => false },
        /** Returning true will prevent the popover from closing when escape is pressed */
        onEscape: { type: Function as PropType<(event: EventTarget) => boolean>, default: () => false },
        /** The method to be called when the enter key is pressed */
        onEnterMethod: { type: Function as PropType<(target: Event) => void>, default: () => {} },
        trigger: { type: HTMLElement as PropType<HTMLElement>, required: true },
        boundary: { type: [Element, String] as PropType<Boundary>, default: 'clippingParents' },
        rootBoundary: { type: String as PropType<RootBoundary>, default: 'viewport'},
        value: { type: File, default: null },
    });
    const emit = defineEmits<{
        (e: 'close'): void
        (e: 'update:updatePopper', fn: () => void): void
    }>();

    // click outside, keyboard, and close behavior
    const popoverEl: Ref<HTMLElement | null> = ref(null);
    function closePopover() {
        isActive.value = false;
        emit('close');
    }
    onClickOutside(popoverEl, function(event: Event) {
        if (props.onClickOutside(event.target as EventTarget)) return;
        closePopover();
    });
    onKeyStroke('Escape', event => {
        if (props.onEscape(event.target as EventTarget)) return;
        closePopover();
    });
    onKeyStroke('Enter', props.onEnterMethod);

    const isFooterArrow = computed(() => {
        return !!useSlots().footer && props.position.indexOf('top') !== -1;
    });

    // Popper.js setup
    const arrowEl: Ref<HTMLElement | null> = ref(null);
    const topOffset = computed(() => {
        const feedbackMessageEl = document.querySelector('#notification-container.active .feedback-element') as HTMLElement;
        const phNavEl = document.getElementById('ph-nav-top');
        const phNavHeight = phNavEl ? phNavEl.offsetHeight : 0;
        return (feedbackMessageEl ? phNavHeight + feedbackMessageEl.offsetHeight : phNavHeight) + 15;
    });
    const popperOptions = computed(() => {
        type CustomModifier = Partial<Modifier<'coverTrigger', {}>>;
        const popperOptions: Partial<OptionsGeneric<StrictModifiers | CustomModifier>> = {
            placement: props.position,
            modifiers: [
                { name: 'offset', options: { offset: props.offset }},
                { name: 'flip', options: { padding: topOffset.value, ...props.flip }},
                { name: 'preventOverflow', options: { padding: 15, boundary: props.boundary, rootBoundary: props.rootBoundary }},
            ],
        };
        if (props.arrow) {
            popperOptions.modifiers!.push({
                name: 'arrow',
                options: { element: arrowEl.value },
            });
        }
        if (props.coverTrigger) {
            popperOptions.modifiers!.push({
                name: 'coverTrigger',
                enabled: true,
                phase: 'main',
                fn({ state, name }) {
                    if (state.modifiersData[name]._skip) return;
                    const height = (state.elements.reference as HTMLElement).offsetHeight;
                    const offset = state.options.modifiers.find(modifier => modifier.name === 'offset');
                    offset.options.offset[1] = -height;
                    state.modifiersData[name]._skip = true;
                    state.reset = true;
                },
            });
        }
        return popperOptions;
    });
    onMounted(async() => {
        await nextTick();
        const popper = createPopper(props.trigger, popoverEl.value as HTMLElement, popperOptions.value);
        isActive.value = true;
        if (props.coverTrigger || props.refreshPosition) popper.update();
        if (props.scrollToView) {
            nextTick(() => (popoverEl.value as HTMLElement).scrollIntoView());
        }
        // necessary to properly flip the popover because the trigger element is passed in as a prop.
        // ideally we'd use a slot instead to avoid this.
        await nextTick();
        popper.update();
        // allow the implementing code to manually update the popper
        emit('update:updatePopper', () => popper.update());
    });
</script>

<style lang="scss" scoped>
    @import '@design/index.scss';

    .popover-enter-active, .popover-leave-active {
        transition: opacity 0.15s;
    }
    .popover-enter-from, .popover-leave-active {
        opacity: 0;
    }
    .v-popover {
        text-align: left;
        min-width: 400px;
        border-radius: 4px;
        border: 1px solid $gray;
        box-shadow: 0 5px 10px rgba(0,0,0,0.2);
        background-color: #ffffff;
        z-index: 215;
        position: absolute;
        cursor: default;
        @include popper-arrows($size: 10px, $color: $state-primary-color, $borderColor: $gray);
        &:focus, &:active {
            outline: none;
        }
        &.v-popover-narrow {
            min-width: 0;
            width: 280px;
        }
        &.v-popover-square-corners {
            border-radius: 0;
        }
        &.v-popover-extra-padding {
            .popover-body {
                padding: 20px 15px;
            }
        }
        &.v-popover-arrowless {
            margin-top: 0;
            margin-bottom: 0;
        }
        .popover-content {
            padding: 0;
        }
        .popover-body {
            padding: 9px 14px;
        }
        .popover-header,
        .popover-footer {
            padding: 15px;
            background-color: $lighter-gray;
        }
        .popover-header {
            border-bottom: 1px solid $gray;
        }
        .popover-footer {
            border-top: 1px solid $gray;
            border-radius: 0 0 4px 4px;
            .v-popover-square-corners & {
                border-radius: none;
            }
        }
        .v-popover-arrow-gray {
            border-color: $lighter-gray;
        }
    }
</style>
