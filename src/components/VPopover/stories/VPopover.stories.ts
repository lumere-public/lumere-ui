import { vueStory } from '@vueStory';
import { VPopover } from '../index';
import SimpleStory from './Simple.story.vue';
import NarrowStory from './Narrow.story.vue';
import EditPopoverStory from './EditPopover.story.vue';
import CoverTriggerStory from './CoverTrigger.story.vue';

export default {
    title: 'Vue/Components/VPopover',
    component: VPopover,
};

export const Simple = vueStory(SimpleStory);
export const Narrow = vueStory(NarrowStory);
export const EditPopover = vueStory(EditPopoverStory);
export const CoverTrigger = vueStory(CoverTriggerStory);
