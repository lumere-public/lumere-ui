import { ref } from 'vue';
import { mount } from '@vue/test-utils';
import { VPopover } from './index';
import { expect } from 'vitest';

describe('testing VPopover.vue', () => {
    const mountVPopover = (props = {}) => {
        return mount({
            components: { VPopover },
            setup() {
                const popoverTrigger = ref(null);
                const showPopover = ref(false);
                function togglePopover() {
                    showPopover.value = !showPopover.value;
                }
                return {
                    popoverTrigger,
                    showPopover,
                    togglePopover,
                    props,
                };
            },
            template: `
                <VPopover v-if="showPopover" @close="togglePopover" :trigger="popoverTrigger" v-bind="props">
                    <template #body>
                        This is a popover.
                    </template>
                </VPopover>
                <a @click="togglePopover" ref="popoverTrigger" class="btn btn-success" id="toggle">Toggle popover</a>
                <button id="outside">Some other content</button>
            `,
        }, { attachTo: document.body });
    };

    describe('testing click outside behavior', () => {
        it('clicking outside closes the popover', async() => {
            const wrapper = mountVPopover();
            await wrapper.find('#toggle').trigger('click');
            expect(wrapper.html()).toContain('This is a popover.');
            const outsideEl = wrapper.find('#outside');
            await outsideEl.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.html()).not.toContain('This is a popover.');
        });
        it('if onClickOutside returns true, the popover is prevented from closing', async() => {
            const wrapper = mountVPopover({ onClickOutside: () => true });
            await wrapper.find('#toggle').trigger('click');
            expect(wrapper.html()).toContain('This is a popover.');
            const outsideEl = wrapper.find('#outside');
            await outsideEl.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.html()).toContain('This is a popover.');
        });
        it('if onClickOutside returns false, the popover closes', async() => {
            const wrapper = mountVPopover({ onClickOutside: () => false });
            await wrapper.find('#toggle').trigger('click');
            expect(wrapper.html()).toContain('This is a popover.');
            const outsideEl = wrapper.find('#outside');
            await outsideEl.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.html()).not.toContain('This is a popover.');
        });
    });

    describe('testing keyboard behavior', () => {
        it('calls the onEnterMethod prop if key is Enter', async() => {
            let count = 0;
            const wrapper = mountVPopover({ onEnterMethod: () => count++ });
            await wrapper.find('#toggle').trigger('click');
            expect(count).toBe(0);
            window.dispatchEvent(new KeyboardEvent('keydown', {
                key: 'Enter',
            }));
            expect(count).toBe(1);
        });

        it('closes the popover if the key is Escape', async() => {
            const wrapper = mountVPopover();
            await wrapper.find('#toggle').trigger('click');
            expect(wrapper.html()).contains('This is a popover.');
            window.dispatchEvent(new KeyboardEvent('keydown', {
                key: 'Escape',
            }));
            await wrapper.vm.$nextTick();
            expect(wrapper.html()).not.contains('This is a popover.');
        });
    });

    describe('testing emits', () => {
        it('emits a close event when the popover is closed', async() => {
            const wrapper = mountVPopover();
            await wrapper.find('#toggle').trigger('click');
            expect(wrapper.html()).contains('This is a popover.');
            const popoverWrapper = wrapper.findComponent(VPopover);
            popoverWrapper.vm.closePopover();
            expect(popoverWrapper.emitted().close).toBeTruthy();
        });
    });
});
