import { vueStory } from '@vueStory';
import { VEditor } from '../';
import SimpleStory from './Simple.story.vue';
import MentionsStory from './Mentions.story.vue';
import SlotsStory from './Slots.story.vue';
import PlaceholderStory from './Placeholder.story.vue';

export default {
    title: 'Vue/Components/VEditor',
    component: VEditor,
};

export const Simple = vueStory(SimpleStory);
export const Mentions = vueStory(MentionsStory);
export const Slots = vueStory(SlotsStory);
export const Placeholder = vueStory(PlaceholderStory);
