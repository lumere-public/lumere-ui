import { mount } from '@vue/test-utils';
import { VEditor } from '../';
import VEditorLinkPopover from '../VEditorLinkPopover.vue';

describe('testing VEditorLinkPopover.vue', function() {
    let popover;
    let protocolInput;
    let pathInput;

    beforeEach(async() => {
        const getWrapper = () => new Promise(resolve => {
            const wrapper = mount({
                components: { VEditor },
                data() {
                    return {
                        description: '<p>Hello</p>',
                    };
                },
                template: `
                    <VEditor v-model="description" @created="returnWrapper"/>
                `,
                methods: {
                    returnWrapper,
                },
            }, { attachTo: document.body });
            function returnWrapper() {
                return resolve(wrapper);
            }
        });

        const wrapper = await getWrapper();
        // focus the editor to render the toolbar
        await wrapper.find('.ProseMirror').trigger('focus');
        const toolbar = await wrapper.getComponent('.toolbar');
        // click the link button to render the VEditorLinkPopover component
        await toolbar.find('#v-editor-toolbar-link-button').trigger('click');

        popover = wrapper.getComponent(VEditorLinkPopover);
        protocolInput = popover.find('select');
        pathInput = popover.findAll('input[type="text"]')[1];
    });
    describe('computed', () => {
        describe('url', () => {
            it('returns the protocol + the path', async() => {
                await pathInput.setValue('lumere.com');
                expect(popover.vm.url).toBe('https://lumere.com');
                await protocolInput.setValue('http://');
                expect(popover.vm.url).toBe('http://lumere.com');
            });
        });
    });
    describe('data', () => {
        describe('path', () => {
            it('http and https protocols entered into the path field are replaced and used to update the protocol data attribute', async() => {
                expect(popover.vm.protocol).toBe('https://');
                await pathInput.setValue('http://lumere.com');
                expect(popover.vm.protocol).toBe('http://');
                expect(popover.vm.path).toBe('lumere.com');

                await pathInput.setValue('https://lumere.com');
                expect(popover.vm.protocol).toBe('https://');
                expect(popover.vm.path).toBe('lumere.com');
            });
        });
    });
});
