import { VEditor } from '../';
import { mount } from '@vue/test-utils';

describe('testing VEditor.vue', () => {
    it('getMentions returns all active mentions', async() => {
        const wrapper = await new Promise(resolve => {
            const wrapper = mount({
                components: { VEditor },
                data() {
                    return {
                        value: '<p>Hello <span data-type="mention" class="mention" data-id="99"></p><ul><li><p><span data-type="mention" class="mention" data-id="1">@Super User 1 ▷</span> </p></li></ul>',
                        mentions: [
                            { id: 1, name: 'Super User 1 ▷'},
                            { id: 2, name: 'User 2'},
                            { id:99, name: 'Bender' },
                        ],
                        getMentions: () => {},
                    };
                },
                template: `
                    <VEditor v-model="value" v-model:getMentions="getMentions" :mentions="mentions" @created="returnWrapper"/>
                `,
                methods: {
                    returnWrapper,
                },
            });
            function returnWrapper() {
                return resolve(wrapper);
            }
        });
        expect(wrapper.vm.getMentions()).toEqual([{ id: 99, name: 'Bender' }, { id: 1, name: 'Super User 1 ▷' }]);
    });
});
