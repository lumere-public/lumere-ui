import { computed } from 'vue';
import { VueRenderer } from '@tiptap/vue-3';
import type { JSONContent } from '@tiptap/core';
import type { Instance as TippyInstance } from 'tippy.js';
import MentionExtension from '@tiptap/extension-mention';
import VMentionList from './VMentionList.vue';
import tippy from 'tippy.js';
export type Mention = {
    id: number | string,
    name: string,
}

export function useMention(mentions: Mention[], onMentionAdded: (mention: Mention) => void) {
    if (!mentions.length) return { mentionInstance: null, getMentions };

    const mentionsById = computed(() => {
        if (!mentions.length) return {};
        return mentions.reduce((acc, mention) => {
            acc[mention.id] = mention;
            return acc;
        }, {} as Record<number | string, Mention>);
    });

    function getMentions(content: JSONContent[] = []) {
        return content.reduce((acc, node) => {
            if (node.content) acc = acc.concat(getMentions(node.content));
            if (node.type === 'mention' && node.attrs?.id) acc.push({ id: parseInt(node.attrs.id, 10), name: mentionsById.value[node.attrs.id].name });
            return acc;
        }, [] as Mention[]);
    }

    const mentionInstance = MentionExtension.configure({
        HTMLAttributes: {
            class: 'mention',
        },
        renderLabel({ options, node }) {
            return options.suggestion.char + mentionsById.value[node.attrs.id].name;
        },
        suggestion: {
            items({ query }) {
                return mentions.filter(({ name }) => name.toLowerCase().startsWith(query.toLowerCase())).slice(0, 10);
            },
            command({ editor, range, props }) {
                editor
                    .chain()
                    .focus()
                    .insertContentAt(range, [
                        {
                            type: 'mention',
                            attrs: props,
                        },
                        {
                            type: 'text',
                            text: ' ',
                        },
                    ])
                    .run();
                onMentionAdded(props as Mention);
            },
            render() {
                let component: VueRenderer;
                let popup: TippyInstance[];

                return {
                    onStart(props) {
                        component = new VueRenderer(VMentionList, {
                            props,
                            editor: props.editor,
                        });

                        popup = tippy('body', {
                            getReferenceClientRect: props.clientRect,
                            appendTo: () => document.body,
                            content: component.element,
                            theme: 'plain',
                            showOnCreate: true,
                            interactive: true,
                            trigger: 'manual',
                            placement: 'bottom-start',
                        });
                    },
                    onUpdate(props) {
                        component.updateProps(props);
                        popup[0].setProps({
                            getReferenceClientRect: props.clientRect,
                        });
                    },
                    onKeyDown(props) {
                        if (component.ref) return component.ref.onKeyDown(props);
                    },
                    onExit() {
                        popup[0].destroy();
                        component.destroy();
                    },
                };
            },
        },
    });
    return {
        mentionInstance,
        getMentions,
    };
}
