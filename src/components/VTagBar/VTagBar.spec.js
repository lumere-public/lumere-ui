import { mount, shallowMount } from '@vue/test-utils';
import { VTagBar } from './index';
import VTagBarTag from './VTagBarTag.vue';
import flushPromises from 'flush-promises';

describe('testing VTagBar.vue', () => {
    describe('Props', () => {
        it('removable - displays a remove button on hover', () => {
            const wrapper = mount(VTagBar, {
                props: {
                    modelValue: null,
                    removable: true,
                    options: [{ id: 1, name: 'High priority' }],
                },
            });
            expect(wrapper.html()).toContain('glyphicons-remove');
        });
    });

    describe('Computed', () => {
        let options;

        beforeEach(() => {
            options = {
                props: {
                    modelValue: [{ id: 1, name: 'High priority' }],
                    options: [
                        { id: 1, name: 'High priority' },
                    ],
                    multiple: true,
                },
            };
        });

        describe('valueById returns a map of value(s) by their trackBy property', () => {
            it('works in multiple select mode', () => {
                const wrapper = shallowMount(VTagBar, options);
                expect(wrapper.vm.valueById).toEqual({
                    1: { id: 1, name: 'High priority' },
                });
            });

            it('works in single select mode', () => {
                options.props.multiple = false;
                options.props.modelValue = { id: 2, name: 'Owned by me' };
                const wrapper = shallowMount(VTagBar, options);
                expect(wrapper.vm.valueById).toEqual({
                    2: { id: 2, name: 'Owned by me' },
                });
            });
        });

        describe('optionsById returns a map of options by their trackBy property', () => {
            it('uses id by default', () => {
                const wrapper = shallowMount(VTagBar, options);
                expect(wrapper.vm.optionsById).toEqual({
                    1: { id: 1, name: 'High priority' },
                });
            });
        });
    });

    describe('Methods', () => {
        describe('toggleSelectedOption', () => {
            it('toggles selected filter in single select mode', async() => {
                const parent = mount({
                    components: { VTagBar },
                    data() {
                        return {
                            value: { id: 1, name: 'High priority' },
                            options: [
                                { id: 1, name: 'High priority' },
                                { id: 2, name: 'Owned by me' },
                                { id: 3, name: 'Active' },
                            ],
                        };
                    },
                    template: '<v-tag-bar v-model="value" :options="options" :multiple="false"/>',
                });
                const wrapper = parent.findComponent(VTagBar);

                // High priority is selected, deselect it directly
                await wrapper.vm.toggleSelectedOption({ id: 1, name: 'High priority' });
                expect(wrapper.emitted()['update:modelValue'][0][0]).toEqual(null);

                // reselect it
                await wrapper.vm.toggleSelectedOption({ id: 1, name: 'High priority' });
                expect(wrapper.emitted()['update:modelValue'][1][0]).toEqual({ id: 1, name: 'High priority' });

                // deselect it by selecting a different option
                await wrapper.vm.toggleSelectedOption({ id: 2, name: 'Owned by me' });
                expect(wrapper.emitted()['update:modelValue'][2][0]).toEqual({ id: 2, name: 'Owned by me'});

                // calling with optional parameter isActive set to false will avoid deselection
                await wrapper.vm.toggleSelectedOption({ id: 2, name: 'Owned by me' }, false);
                expect(wrapper.emitted()['update:modelValue'][3][0]).toEqual({ id: 2, name: 'Owned by me' });

                // calling with optional parameter isActive set to true will avoid selection
                await wrapper.vm.toggleSelectedOption({ id: 2, name: 'Owned by me' }); // deselect it
                expect(wrapper.emitted()['update:modelValue'][4][0]).toEqual(null);
                await wrapper.vm.toggleSelectedOption({ id: 2, name: 'Owned by me' }, true);
                expect(wrapper.emitted()['update:modelValue'][4][0]).toEqual(null);
            });

            it('toggles selected filters in multiple select mode', async() => {
                const parent = mount({
                    components: { VTagBar },
                    data() {
                        return {
                            value: [{ id: 1, name: 'High priority' }],
                            options: [
                                { id: 1, name: 'High priority' },
                                { id: 2, name: 'Owned by me' },
                                { id: 3, name: 'Active' },
                            ],
                        };
                    },
                    template: '<v-tag-bar v-model="value" :options="options" :multiple="true"/>',
                });
                const wrapper = parent.findComponent(VTagBar);

                // High priority is selected, also select Owned by me
                await wrapper.vm.toggleSelectedOption({ id: 2, name: 'Owned by me' });
                expect(wrapper.emitted()['update:modelValue'][0][0]).toEqual([{ id: 1, name: 'High priority' }, { id: 2, name: 'Owned by me' }]);

                // deselect High priority
                await wrapper.vm.toggleSelectedOption({ id: 1, name: 'High priority' });
                expect(wrapper.emitted()['update:modelValue'][1][0]).toEqual([{ id: 2, name: 'Owned by me' }]);

                // deselect Owned by me, nothing is selected now
                await wrapper.vm.toggleSelectedOption({ id: 2, name: 'Owned by me' });
                expect(wrapper.emitted()['update:modelValue'][2][0]).toEqual([]);

                // calling with optional parameter isActive set to true will avoid selection
                await wrapper.vm.toggleSelectedOption({ id: 1, name: 'High priority' }, true);
                expect(wrapper.emitted()['update:modelValue'][3][0]).toEqual([]);

                // calling with optional parameter isActive set to false will avoid deselection
                await wrapper.vm.toggleSelectedOption({ id: 1, name: 'High priority' }); // select it
                expect(wrapper.emitted()['update:modelValue'][4][0]).toEqual([{ id: 1, name: 'High priority' }]);
                await wrapper.vm.toggleSelectedOption({ id: 1, name: 'High priority' }, false);
                expect(wrapper.emitted()['update:modelValue'][5][0]).toEqual([{ id: 1, name: 'High priority' }]);
            });
        });

        describe('toggleSelectedOptionIfRemoved', () => {
            describe('when an active option is removed from the options prop by an ancestor component, it gets removed from the v-model value', () => {
                it('single select mode', async() => {
                    const wrapper = mount({
                        components: { VTagBar },
                        data() {
                            return {
                                tags: { id: 1, name: 'High priority' },
                                options: [
                                    { id: 1, name: 'High priority' },
                                    { id: 2, name: 'Owned by me' },
                                    { id: 3, name: 'Active' },
                                ],
                            };
                        },
                        template: '<v-tag-bar v-model="tags" :options="options" :multiple="false"/>',
                    });
                    wrapper.vm.options = wrapper.vm.options.filter(option => option.id !== 1);
                    await flushPromises();
                    expect(wrapper.vm.tags).toEqual(null);
                });

                it('multiple select mode', async() => {
                    const wrapper = mount({
                        components: { VTagBar },
                        data() {
                            return {
                                tags: [{ id: 1, name: 'High priority' }],
                                options: [
                                    { id: 1, name: 'High priority' },
                                    { id: 2, name: 'Owned by me' },
                                    { id: 3, name: 'Active' },
                                ],
                            };
                        },
                        template: '<v-tag-bar v-model="tags" :options="options" :multiple="true"/>',
                    });
                    wrapper.vm.options = wrapper.vm.options.filter(option => option.id !== 1);
                    await flushPromises();
                    expect(wrapper.vm.tags).toEqual([]);
                });
            });
        });
    });

    describe('Events', () => {
        let parent;
        let tagBar;
        let firstTag;

        beforeEach(() => {
            parent = mount({
                components: { VTagBar },
                data() {
                    return {
                        tags: [],
                        options: [
                            { id: 1, name: 'High priority' },
                            { id: 2, name: 'Owned by me' },
                            { id: 3, name: 'Active' },
                        ],
                    };
                },
                template: '<v-tag-bar v-model="tags" :options="options" :multiple="true" :removable="true"/>',
            });
            tagBar = parent.findComponent(VTagBar);
            firstTag = parent.findComponent(VTagBarTag);
        });

        it('@click - emitted when a tag is clicked with the tag and its state', async() => {
            // active
            await firstTag.trigger('click');
            expect(parent.vm.tags).toEqual([{ id: 1, name: 'High priority'}]);
            expect(tagBar.emitted().click[0]).toEqual([{ id: 1, name: 'High priority'}, true]);

            // inactive
            await firstTag.trigger('click');
            expect(parent.vm.tags).toEqual([]);
            expect(tagBar.emitted().click[1]).toEqual([{ id: 1, name: 'High priority'}, false]);
        });

        it('@remove - emitted with when the remove button for a tag is clicked with the tag', async() => {
            const removeButton = firstTag.find('.glyphicons-remove');
            await removeButton.trigger('click');
            expect(tagBar.emitted().remove[0]).toEqual([{ id: 1, name: 'High priority'}]);
        });
    });
});
