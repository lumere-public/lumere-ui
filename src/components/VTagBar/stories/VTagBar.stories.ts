import { vueStory } from '@vueStory';
import { VTagBar } from '../index';
import SingleStory from './Single.story.vue';
import MultipleStory from './Multiple.story.vue';
import RemovableStory from './Removable.story.vue';
import NotToggleableStory from './NotToggleable.story.vue';

export default {
    title: 'Vue/Components/VTagBar',
    component: VTagBar,
};

export const Single = vueStory(SingleStory);
export const Multiple = vueStory(MultipleStory);
export const Removable = vueStory(RemovableStory);
export const NotToggleable = vueStory(NotToggleableStory);
