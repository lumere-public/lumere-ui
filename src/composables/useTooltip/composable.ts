import tippy from 'tippy.js';
import { ref, unref } from 'vue';
import { useElement } from '~/utils/composables/useElement';
import type { Instance, Placement } from 'tippy.js';
import type { Ref } from 'vue';
import './useTooltip.css';

export function useTooltip(el: HTMLElement | Ref<HTMLElement | null> | null, content = '', placement?: Placement) {
    // this composable can be leveraged in a directive as long as lifecycle hooks are avoided
    const tippyInstance = ref<Instance>();
    const title = ref(content);

    useElement(
        el,
        el => {
            if (!el.title || el.title !== title.value) el.title = title.value;
            tippyInstance.value = tippy(unref(el)!, {
                content: title.value,
                placement,
                duration: 150,
                onShown() {
                    el.title = '';
                },
                onHidden() {
                    el.title = title.value;
                },
            });
        },
        () => {
            tippyInstance.value?.destroy();
        },
    );

    return tippyInstance;
}
