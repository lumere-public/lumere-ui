import { vueStory } from '@vueStory';
import ComposableStory from './Composable.story.vue';
import DirectiveStory from './Directive.story.vue';

export default {
    title: 'Vue/Composables and Directives/useTooltip',
    decorators: [() => ({ template: '<div style="padding: 2em 2em;"><story /></div>' })],
};


export const Composable = vueStory(ComposableStory);
export const Directive = vueStory(DirectiveStory);
