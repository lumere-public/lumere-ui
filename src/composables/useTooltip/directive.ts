import { useTooltip } from '.';
import type { FunctionDirective} from 'vue';
import type { Placement } from 'tippy.js';

export const vTooltip: FunctionDirective = (el, binding) => {
    useTooltip(el, binding.value, binding.arg as Placement);
};
