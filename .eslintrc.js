module.exports = {
    env: {
        browser: true,
        commonjs: false,
        es6: true,
        node: true,
        jest: true,
        'vue/setup-compiler-macros': true,
    },
    extends: ['eslint:recommended', 'plugin:vue/vue3-essential'],
    parserOptions: {
        parser: '@typescript-eslint/parser',
    },
    plugins: ['@typescript-eslint', 'jest', 'eslint-plugin-html'],
    ignorePatterns: ['src/design/vendors/*'],
    rules: {
        /* Possible errors */
        /* https://eslint.org/docs/rules/#possible-errors */
        'no-console': ['error',
            { allow: ['warn', 'error', 'info'] },
        ],

        /* Best Practices */
        /* https://eslint.org/docs/rules/#best-practices */
        curly: [
            'error',
            'multi-line',
        ],
        'dot-notation': 'error',
        eqeqeq: 'error',
        'no-else-return': 'error',
        'no-multi-spaces': 'error',
        'no-return-assign': 'error',

        /* Variables */
        /* https://eslint.org/docs/rules/#variables */
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': ['error'],

        /* Stylistic Issues */
        /* https://eslint.org/docs/rules/#stylistic-issues */
        'array-bracket-spacing': ['error', 'never'],
        'array-element-newline': ['error', 'consistent'],
        'brace-style': [
            'error',
            '1tbs',
            { allowSingleLine: true },
        ],
        'comma-dangle': ['error', {
            arrays: 'always-multiline',
            objects: 'always-multiline',
            imports: 'always-multiline',
            exports: 'always-multiline',
            functions: 'ignore',
        }],
        'comma-spacing': [
            'error',
            { before: false, after: true },
        ],
        'comma-style': ['error', 'last'],
        'computed-property-spacing': ['error', 'never'],
        'consistent-this': ['error', 'self'],
        'func-call-spacing': 'off',
        '@typescript-eslint/func-call-spacing': ['error'],
        'function-paren-newline': ['error', 'multiline'],
        'implicit-arrow-linebreak': ['error', 'beside'],
        indent: [
            'error',
            4,
            { SwitchCase: 1 },
        ],
        'keyword-spacing': [
            'error',
            { before: true, after: true },
        ],
        'linebreak-style': ['error', 'unix'],
        'no-continue': 'error',
        'no-lonely-if': 'error',
        'no-multi-assign': 'error',
        'no-multiple-empty-lines': 'error',
        'no-nested-ternary': 'error',
        'no-trailing-spaces': 'error',
        'no-unneeded-ternary': 'error',
        'no-whitespace-before-property': 'error',
        'object-curly-newline': [
            'error',
            { multiline: true, consistent: true },
        ],
        'one-var': ['error', 'never'],
        'quote-props': ['error', 'as-needed'],
        quotes: [
            'error',
            'single',
        ],
        semi: ['error', 'always'],
        'semi-spacing': 'error',
        'semi-style': ['error', 'last'],
        'space-before-blocks': 'error',
        'space-before-function-paren': ['error', 'never'],
        'space-in-parens': ['error', 'never'],
        'space-infix-ops': 'error',
        'space-unary-ops': 'error',
        'switch-colon-spacing': 'error',

        /* ECMAScript 6 */
        /* https://eslint.org/docs/rules/#ecmascript-6 */
        'arrow-spacing': 'error',
        'no-confusing-arrow': 'error',
        '@typescript-eslint/no-duplicate-imports': 'error',
        'no-useless-computed-key': 'error',
        'no-useless-rename': 'error',
        'object-shorthand': 'error',
        'prefer-const': ['error', {
            destructuring: 'all',
            ignoreReadBeforeAssign: true,
        }],
        'prefer-template': 'error',
        'rest-spread-spacing': ['error', 'never'],
        'template-curly-spacing': 'error',

        /* eslint-plugin-vue */
        /* https://eslint.vuejs.org/rules/ */
        'vue/html-closing-bracket-newline': ['error', {
            singleline: 'never',
            multiline: 'never',
        }],
        'vue/component-name-in-template-casing': ['error', 'PascalCase', {
            registeredComponentsOnly: false,
        }],
        'vue/html-closing-bracket-spacing': ['error', {
            selfClosingTag: 'never',
        }],
        'vue/html-end-tags': 'error',
        'vue/html-quotes': 'error',
        'vue/html-self-closing': ['error', {
            html: {
                void: 'any',
                normal: 'any',
                component: 'always',
            },
        }],
        'vue/multiline-html-element-content-newline': 'error',
        'vue/mustache-interpolation-spacing': 'error',
        'vue/no-multi-spaces': 'error',
        'vue/no-spaces-around-equal-signs-in-attribute': 'error',
        'vue/no-template-shadow': 'error',
        'vue/prop-name-casing': ['error', 'camelCase'],
        'vue/require-default-prop': 'error',
        'vue/require-prop-types': 'error',
        'vue/this-in-template': 'error',
        'vue/v-bind-style': ['error', 'shorthand'],
        'vue/v-on-style': ['error', 'shorthand'],
        /* Vue uncategorized */
        'vue/array-bracket-spacing': ['error', 'never'],
        'vue/brace-style': [
            'error',
            '1tbs',
            { allowSingleLine: true },
        ],
        'vue/eqeqeq': 'error',
        'vue/no-restricted-syntax': 'error',
        'vue/script-indent': ['error', 4, {
            baseIndent: 1,
            switchCase: 1,
            ignores: [],
        }],
        'vue/v-on-function-call': 'error',


        /* eslint-plugin-jest */
        /* https://github.com/jest-community/eslint-plugin-jest */
        'jest/prefer-called-with': 'error',
        'jest/no-identical-title': 'error',
        'jest/no-disabled-tests': 'error',
        'jest/prefer-comparison-matcher': 'error',
        'jest/prefer-equality-matcher': 'error',
    },
    overrides: [
        {
            files: ['*.vue'],
            rules: {
                indent: 'off',
            },
        },
        {
            files: ['build/**/*.js'],
            env: {
                amd: true,
                node: true,
            },
            globals: {
                __webpack_public_path__: 'readonly',
            },
        },
    ],
    settings: {
        'import/resolver': {
            webpack: {
                config: 'build/webpack.base.config.js',
            },
        },
    },
};
