/// <reference types="vitest" />
import { defineConfig } from 'vite';
import * as path from 'path';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    plugins: [vue({
        template: {
            compilerOptions: {
                whitespace: 'preserve',
            },
        },
    })],
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/index'),
            name: 'LumereUI',
            formats: ['es'],
        },
        rollupOptions: {
            external: ['vue'],
            output: {
                globals: {
                    vue: 'Vue',
                },
                preserveModules: true,
                entryFileNames({ name }) {
                    return `${name}.js`;
                },
            },
        },
    },
    resolve: {
        extensions: ['.vue', '.ts'],
        alias: [
            { find: '@design', replacement: path.resolve(__dirname, './src/design') },
            { find: '~', replacement: path.resolve(__dirname, './src') },
        ],
    },
    test: {
        globals: true,
        environment: 'jsdom',
    },
});
