# lumere-ui
Lumere's reusable component library and utilities.

## Getting Started
### Installation
To install dependencies, run:

```
npm ci
```

This will delete your `node_modules` folder if it exists for a clean state, and install dependencies according the package-lock file. It will also not add unnecessary/unintended changes to the lock file.

If you need to install new dependencies or want changes to the lock file, run:

```
npm install
```

### Running Storybook
We use [Storybook](https://storybook.js.org/) as living documentation to view our components and their various states. We also use Storybook to develop components in isolation.

To run:

```
npm run storybook
```
This will open up Storybook on `localhost:6006`
### Running Tests
#### Unit Tests
We use Vitest for our tests. By default, when you push your code to Gitlab, the tests will automatically run.

To manually run all the tests:
```
npm run test
```

## Contributing
### Commits
We use [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary) to verify that commits are structured in a particular way. This allows us to keep an organized and meaningful git history.

Each commit should follow this basic sturcture:
```
type(scope?): subject

optional body

optional footer
```
- The `type` describes the __category__ of the change
    - The most important and common types are `feat` and `fix` as they distinguish between new features and corrections
        - `feat` correlates with `MINOR` semantic versioning
        - `fix` correlates with `PATCH` semantic versioning
    - Other commonly used types are `chore`, `docs`, and `test`
- The `scope` describes the __module or component__ affected by the changed
- The `subject` gives a brief description of the change
    - Good practices to follow for the subject:
        - Start the description with an active verb such as `undo` or `add`
        - Avoid repeating information from the type and scope
    - If further explanation is needed, describe the details of the change in the `body` of the commit

Example:
```
feat(VTagBar): add disabled prop

This allows the consumer to have more configurable control

```

To indicate a __Breaking Change__, include `BREAKING CHANGE` at the footer of the commit message and/or a `!` after the type/scope
    - This will correlate with `MAJOR` semantic versioning

Example:
```
fix(Button)!: change disabled prop to required

BREAKING CHANGE: the disabled prop is now used for determining which event to emit
```

### Tree shaking
We should aim to allow any new code we add to be tree shakeable. If in doubt, refer to the [consumer](/consumer) directory for instructions and a ready made build process for testing tree shaking.

### Styles
The component should not have references to any internal website classnames (prevent leakage), and any component styles should be within a scoped styled block.

Also when styling a component, be sure that it utilizes variables for colors, spacing, font-sizes where possible. This will help the components maintain a cohesive UI.

Example:
```scss
<style lang="scss" scoped>
    @import '@design/index.scss';
    /* This imports the variables and mixins into this component */

    /* Add the rest of the component styles */
</style>
```

If a class is written more than 3 times to style a particular component (such as adding a muted color to an h1 tag), it should probably discussed with the team to make it/incorporate it into a reusable component.

## Consumption

To use in your own project, install the libary via npm

```
npm install --save @lumere/lumere-ui
```

Then, in the import the components you wish to use from the library

```
import { VTagBar } from '@lumere/lumere-ui';
```
### Styles

#### Pre-requisites
You will need to include tags for the following libraries in your app:

- Bootstrap3
- Glyphicons
- Iconic
- Graphik

We also rely on Bootstrap 3 for some of our css classes. Because the output of Bootstrap 3 customizer is CSS (and not SCSS), it is not overwritable.
