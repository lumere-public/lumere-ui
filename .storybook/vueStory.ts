import type { Component } from 'vue';

type SourcedComponent = Component & {
    __source?: string;
}

export function vueStory(StoryComponent: SourcedComponent) {
    const storyExport = () => StoryComponent;
    storyExport.story = {
        parameters: {
            source: StoryComponent.__source, // added by pluginVueSource
            docs: {
                source: {
                    code: StoryComponent.__source,
                },
            },
        },
    };
    return storyExport;
}

