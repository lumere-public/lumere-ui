import type { Plugin } from 'vite';
import fs from 'fs';

export function pluginVueSource(): Plugin {
    return {
        name: 'vite:vue-source',
        async transform(src: string, id: string) {
            if (/\.(vue)$/.test(id)) {
                const fileContent = fs.readFileSync(id, 'utf8');
                return `${src};_sfc_main.__source = ${JSON.stringify(fileContent)}`;
            }
        },
    };
}
