import { addDecorator } from '@storybook/vue3';
import './vendors/bootstrap.min.css';
import './vendors/fonts/glyphicons.css';

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
};

addDecorator(() => ({ template: '<div style="padding: 10px;"><story/></div>' }));
