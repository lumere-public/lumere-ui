import path from 'path';
import vue from '@vitejs/plugin-vue';
import { pluginVueSource } from './pluginVueSource';
import { mergeConfig } from 'vite';
import type { UserConfig, Plugin } from 'vite';

module.exports = {
    "stories": [
        "../src/**/*.stories.mdx",
        "../src/**/*.stories.@(js|jsx|ts|tsx)"
    ],
    "addons": [
        "@storybook/addon-links",
        "@storybook/addon-essentials"
    ],
    "framework": "@storybook/vue3",
    "core": {
        "builder": "storybook-builder-vite"
    },
    async viteFinal(config: UserConfig) {
        const pluginVueIndex = (<Plugin[]>config.plugins).findIndex(plugin => plugin.name === 'vite:vue');

        config.plugins![pluginVueIndex] = vue({
            template: {
                compilerOptions: {
                    whitespace: 'preserve',
                },
            },
        });

        return mergeConfig(config, {
            plugins: [pluginVueSource()],
            resolve: {
                alias: {
                    '@design': path.resolve(__dirname, '../src/design'),
                    '@vueStory': path.resolve(__dirname, './vueStory'),
                    '~': path.resolve(__dirname, '../src'),
                }
            },
        });
    },
};
